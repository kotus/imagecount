
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;


/**
 * Created by k.buhantsev on 09.10.2014.
 */
public class WorkWithHTTP {

    public static String getDataFromURL(String url) throws IOException{

        String site = "";
        String tempString = "";

        URL siteUrl = new URL(url);
        BufferedReader br = new BufferedReader(new InputStreamReader(siteUrl.openConnection().getInputStream()));

        while ((tempString = br.readLine())!= null){
            site = site.concat(tempString+"\n");
        }

        br.close();

        return site;

    };


}
