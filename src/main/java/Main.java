import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.XPatherException;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by k.buhantsev on 09.10.2014.
 */
public class Main {

    public static void main(String[] args) throws IOException, XPathExpressionException, ParserConfigurationException,
              XPatherException {

        String url = inputString("Input URL: ");
        String path = WorkWithHTTP.getDataFromURL(url);

        HtmlCleaner cleaner = new HtmlCleaner();
        TagNode html = cleaner.clean(path);

        Object[] tags = html.evaluateXPath("//img[@src]");

        if (tags.length != 0) {
            System.out.println("Images quantity is :" + tags.length);
        } else {
            return;
        }

        for (Object tag : tags) {
            TagNode aTag = (TagNode) tag;
            String href = aTag.getAttributeByName("src").trim();
            System.out.println(href);
        }

    }

    private static String inputString(String message){

        String result = "";
        Scanner sc = new Scanner(System.in);

        System.out.print(message);
        if (sc.hasNextLine()){
            return sc.nextLine();
        }

        return result;

    }

}
